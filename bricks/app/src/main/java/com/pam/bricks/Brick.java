package com.pam.bricks;

/**
 * @author Mikołaj Kalata, Bartosz Kamński
 * model representing brick
 * */
public class Brick {
    private Boolean isVisible;
    public Integer width, height, row, column;

    public Brick(Integer width, Integer height, Integer row, Integer column) {
        this.isVisible = true;
        this.width = width;
        this.height = height;
        this.row = row;
        this.column = column;
    }

    public Boolean getVisible() {
        return isVisible;
    }

    public void setVisible(Boolean visible) {
        isVisible = visible;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getRow() {
        return row;
    }

    public void setRow(Integer row) {
        this.row = row;
    }

    public Integer getColumn() {
        return column;
    }

    public void setColumn(Integer column) {
        this.column = column;
    }
}
