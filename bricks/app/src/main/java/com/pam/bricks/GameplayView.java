package com.pam.bricks;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Handler;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;

import java.util.Random;

/**
 * @author Mikołaj Kalata, Bartosz Kamiński
 * Main class for drawing the board and handling the gameplay
 */
public class GameplayView extends View {

    Context context;
    float ballY, ballX;
    Velocity velocity = new Velocity(15, 19); // x, y must be different so ball won't move in 45 degree angle
    Handler handler;
    final long DELAY = 20;
    Runnable runnable;
    Paint textPaint = new Paint(); //points
    Paint healthPaint = new Paint(); //health
    Paint brickPaint = new Paint(); //bricks
    float TEXT_SIZE = 125;
    float paddleX, paddleY;
    float oldX, oldPaddleX; //for repositioning the paddle
    int points = 0;
    int life = 3;
    Bitmap ball, paddle;
    int screenWidth, screenHeight;
    int ballWidth, ballHeight;
    //todo/optional add some sound effects
    Random random;
    Brick[] bricks = new Brick[30];
    int numBricks = 0;
    int numBrokenBrick = 0;
    boolean gameOver = false;


    /**
     * Constructor of main game class
     * It sets basic fields attributes, like positions and colors,
     * creates brick grid
     *
     * @param context The context of the application
     */
    public GameplayView(Context context) {
        super(context);
        this.context = context;
        ball = BitmapFactory.decodeResource(getResources(), R.drawable.ball);
        paddle = BitmapFactory.decodeResource(getResources(), R.drawable.paddle);
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                invalidate(); //redraw view
            }
        };

        //colors
        textPaint.setColor(Color.WHITE);
        textPaint.setTextSize(TEXT_SIZE);
        textPaint.setTextAlign(Paint.Align.LEFT);
        healthPaint.setColor(Color.GREEN);
        healthPaint.setTextSize(TEXT_SIZE);
        healthPaint.setTextAlign(Paint.Align.RIGHT);
        brickPaint.setColor(Color.RED);

        //screen dimension
        Display display = ((Activity) getContext()).getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        screenWidth = size.x;
        screenHeight = size.y;

        //initiate ball position (random) + dimensions
        random = new Random();
        ballX = random.nextInt(screenWidth - 50);
        ballY = screenHeight / 3;
        ballWidth = ball.getWidth();
        ballHeight = ball.getHeight();

        //initiate paddel pos
        paddleX = screenWidth / 2 - paddle.getWidth() / 2;
        paddleY = (screenHeight * 4) / 5;
        createBrickGrid();

        // todo/optional prepare sound effects
    }


    /**
     * Creates a 2D grid of bricks for the game
     * The grid consists of 8 columns and 3 rows, totaling 24 bricks
     * Each brick has a fixed width and height, calculated based on screen dimensions
     */
    private void createBrickGrid() {
        int brickWidth = screenWidth / 8;
        int brickHeight = screenHeight / 16;
        for (int col = 0; col < 8; col++) {
            for (int row = 0; row < 3; row++) {
                bricks[numBricks] = new Brick(brickWidth, brickHeight, row, col);
                numBricks++;
            }
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //check for win condition
        checkWinCondition();

        //canvas color
        canvas.drawColor(Color.BLACK);

        //ball movement
        moveBall();

        //bounce the ball when reaches board boundaries
        handleBallReachesBoundaries();

        //ball goes under the paddle
        handleBallMissThePaddle();

        //ball paddle collision
        handleBallPaddleCollision();

        //draw the ball
        drawBall(canvas);

        //draw the paddle
        drawPaddle(canvas);

        //draw the visible bricks
        drawVisibleBricks(canvas);

        //draw points
        drawPoints(canvas);

        //draw life
        drawLife(canvas);

        //handle ball colision with bricks
        handleBallBrickCollision();
    }

    /**
     * Check if the game has been won by the player
     * If the game isn't over, delay the game loop by the specified DELAY value (in milliseconds)
     */
    private void checkWinCondition() {
        if (numBrokenBrick == numBricks) {
            gameOver = true;
        }
        if (!gameOver) {
            handler.postDelayed(runnable, DELAY);
        }
    }

    /**
     * Draw the ball on the canvas
     *
     * @param canvas The canvas to draw the ball on
     */
    private void drawBall(Canvas canvas) {
        canvas.drawBitmap(ball, ballX, ballY, null);
    }

    /**
     * Draw the paddle on the canvas
     *
     * @param canvas The canvas to draw the paddle on
     */
    private void drawPaddle(Canvas canvas) {
        canvas.drawBitmap(paddle, paddleX, paddleY, null);
    }

    /**
     * Draw the visible bricks on the canvas
     *
     * @param canvas The canvas to draw the bricks on
     */
    private void drawVisibleBricks(Canvas canvas) {
        for (int i = 0; i < numBricks; i++) {
            if (bricks[i].getVisible()) {
                canvas.drawRect(
                        bricks[i].column * bricks[i].width + 1,
                        bricks[i].row * bricks[i].height + 1,
                        bricks[i].column * bricks[i].width + bricks[i].width - 1,
                        bricks[i].row * bricks[i].height + bricks[i].height - 1, brickPaint
                );
            }
        }
    }

    /**
     * Draw the player's current points on the canvas
     *
     * @param canvas The canvas to draw the points on
     */
    private void drawPoints(Canvas canvas) {
        canvas.drawText("" + points, 20, TEXT_SIZE, textPaint);
    }

    /**
     * Draw the player's remaining life on the canvas
     *
     * @param canvas The canvas to draw the health bar on
     */
    private void drawLife(Canvas canvas) {
        canvas.drawText("" + life, screenWidth * 0.9f, TEXT_SIZE, healthPaint);
    }

    /**
     * Detect and handle ball collisions with bricks
     * Update the game state accordingly (increase points, decrease brick visibility)
     */
    private void handleBallBrickCollision() {
        for (int i = 0; i < numBricks; i++) {
            if (bricks[i].getVisible()) {
                double brickLeftBoundary = bricks[i].column * bricks[i].width;
                double brickRightBoundary = brickLeftBoundary + bricks[i].width;
                double brickTopBoundary = bricks[i].row * bricks[i].height;
                double brickBottomBoundary = brickTopBoundary + bricks[i].height;

                //if ball enters/touches brick
                if (ballX + ballWidth >= brickLeftBoundary
                        && ballX <= brickRightBoundary
                        && ballY <= brickBottomBoundary
                        && ballY >= brickTopBoundary) {

                    // Determine collision side
                    double prevBallX = ballX - velocity.getX();
                    double prevBallY = ballY - velocity.getY();

                    boolean ballCameFromLeft = prevBallX + ballWidth <= brickLeftBoundary;
                    boolean ballCameFromRight = prevBallX >= brickRightBoundary;
                    boolean ballCameFromTop = prevBallY + ballHeight <= brickTopBoundary;
                    boolean ballCameFromBottom = prevBallY >= brickBottomBoundary;

                    if (ballCameFromTop || ballCameFromBottom) {
                        velocity.setY(velocity.getY() * -1);
                    } else if (ballCameFromLeft || ballCameFromRight) {
                        velocity.setX(velocity.getX() * -1);
                    }

                    //todo sound of hit
                    bricks[i].setVisible(false);
                    points += 5;
                    numBrokenBrick++;
                    if (numBrokenBrick == 24) {
                        handleGameOver();
                    }
                }
            }
        }
    }

    /**
     * Bounces the ball when reaches boundaries, changing X or Y velocity
     * depending if ball did hit top or left/right boundary
     */
    private void handleBallReachesBoundaries() {
        if ((ballX >= screenWidth - ball.getWidth()) || ballX <= 0) {
            velocity.setX(velocity.getX() * -1);
        }
        if (ballY <= 0) {
            velocity.setY(velocity.getY() * -1);
        }
    }

    /**
     * Handle user touch input to move the paddle
     *
     * @param event The MotionEvent object containing information about the touch event
     * @return true if the touch event is handled
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float contactX = event.getX();
        float contactY = event.getY();

        if (contactY >= paddleY) {
            int action = event.getAction();
            if (action == MotionEvent.ACTION_DOWN) {
                oldX = event.getX();
                oldPaddleX = paddleX;
            }
            if (action == MotionEvent.ACTION_MOVE) {
                float move = oldX - contactX;
                float newPaddleX = oldPaddleX - move;
                if (newPaddleX <= 0) {
                    paddleX = 0;
                } else if (newPaddleX >= screenWidth - paddle.getWidth()) {
                    paddleX = screenWidth - paddle.getWidth();

                } else {
                    paddleX = newPaddleX;
                }
            }
        }
        return true;
    }

    /**
     * Missing the paddle - decrease life (handle gameover), reset ball cords
     */
    private void handleBallMissThePaddle() {
        if (ballY > paddleY + paddle.getHeight()) {
            //todo sound of missing

            //decrease life
            life--;
            if (life < 1) {
                gameOver = true;
                handleGameOver();
            }

            //reset ball cord
            ballX = random.nextInt(screenWidth - ball.getWidth() - 1) + 1;
            ballY = screenHeight / 3;

            // velocity x and y must be different so ball won't move in 45 degree angle
            velocity.setX(randomVelocityX());
            velocity.setY(33);

        }
    }

    /**
     * bounce the ball back with increased velocity
     */
    private void handleBallPaddleCollision() {
        if ((ballX + ball.getWidth()) >= paddleX // right edge ball vs left edge paddle
                && ballX <= (paddleX + paddle.getWidth()) // left edge ball vs right edge paddle
                && (ballY + ball.getHeight()) >= paddleY // ball bottom edge vs paddle top
                && (ballY + ball.getHeight()) <= (paddleY + paddle.getHeight())) { // ball bottom vs bottom paddle edge){
            //todo sound of hit
            velocity.setX(velocity.getX() + 25);
            velocity.setY((velocity.getY() + 1) * -1);
        }
    }

    /**
     * When life reaches 0, finish the game by calling the GameOver class, pass the score
     */
    private void handleGameOver() {
        handler.removeCallbacksAndMessages(null);
        Intent intent = new Intent(context, GameOver.class);
        intent.putExtra("points", points);
        context.startActivity(intent);
        ((Activity) context).finish();
    }

    /**
     * Change velocity with random values
     */
    private int randomVelocityX() {
        int[] values = {-35, -30, -25, 25, 30, 35};
        int index = random.nextInt(6);
        return values[index];
    }

    /**
     * Ball movement
     */
    private void moveBall() {
        ballX += velocity.getX();
        ballY += velocity.getY();
        System.out.println(ballX);
    }
}
