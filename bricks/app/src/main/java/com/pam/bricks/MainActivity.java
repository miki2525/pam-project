package com.pam.bricks;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

/**
 * @author Mikołaj Kalata, Bartosz Kamński
 * loads the main layout
 */

public class MainActivity extends AppCompatActivity {

    /**
     * method sets up the activity with the layout from activity_main.xml and keeps the device's screen on for as long as the activity is in the foreground.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    public void playGame(View view) {
        GameplayView gameplayView = new GameplayView(this);
        setContentView(gameplayView);
    }
}