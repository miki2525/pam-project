package com.pam.bricks;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

/**
 * @author Mikołaj Kalata, Bartosz Kamński
 * class for displaying points. restarts/exits the game
 * */
public class GameOver extends AppCompatActivity {

    TextView tvPoints;
    TextView result;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_over);
        tvPoints = findViewById(R.id.tvPoints);
        result = findViewById(R.id.result);
        int points = getIntent().getExtras().getInt("points");
        tvPoints.setText(points + getString(R.string.pointsAbr));

        String message;
        if (points == 120){
            message = getString(R.string.congrats);
        } else {
            message = getString(R.string.try_again);
        }
        result.setText(message);
    }

    /**
     * @param view
     * restart the game
     * */
    public void restart(View view){
        Intent intent = new Intent(GameOver.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * @param view
     * finish the game
     * */
    public void exit(View view){
        finish();
    }
}
