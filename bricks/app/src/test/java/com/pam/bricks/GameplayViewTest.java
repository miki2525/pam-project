package com.pam.bricks;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.MotionEvent;

import androidx.test.core.app.ApplicationProvider;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowActivity;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)
public class GameplayViewTest {

    private GameplayView gameplayView;
    private Context context;

    @Before
    public void setup() {
        context = ApplicationProvider.getApplicationContext();
        gameplayView = new GameplayView(context);
    }

    @Test
    public void testDelayValue() {
        assertEquals(20, gameplayView.DELAY);
    }

    @Test
    public void testVelocityAttributes() {
        assertEquals(15, gameplayView.velocity.getX());
        assertEquals(19, gameplayView.velocity.getY());
    }

    @Test
    public void testCreateBrickGrid() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        Method createBrickGrid = GameplayView.class.getDeclaredMethod("createBrickGrid");
        createBrickGrid.setAccessible(true);
        createBrickGrid.invoke(gameplayView);

        Field numBricksField = GameplayView.class.getDeclaredField("numBricks");
        numBricksField.setAccessible(true);

        int numBricks = (int) numBricksField.get(gameplayView);
        assertEquals(24, numBricks);
    }

    @Test
    public void testCheckWinCondition() throws Exception {
        gameplayView.numBrokenBrick = 5;
        gameplayView.numBricks = 5;

        Method method = GameplayView.class.getDeclaredMethod("checkWinCondition");
        method.setAccessible(true);
        method.invoke(gameplayView);

        assertTrue(gameplayView.gameOver);
    }

    @Test
    public void testHandleBallBrickCollision() throws Exception {
        // Assume that the first brick is visible and the ball is at the same location
        gameplayView.bricks[0].setVisible(true);
        gameplayView.ballX = 0;
        gameplayView.ballY = 0;

        Method method = GameplayView.class.getDeclaredMethod("handleBallBrickCollision");
        method.setAccessible(true);
        method.invoke(gameplayView);

        assertFalse(gameplayView.bricks[0].getVisible());
        assertEquals(5, gameplayView.points);
        assertEquals(1, gameplayView.numBrokenBrick);
    }

    @Test
    public void testHandleBallReachesBoundaries() throws Exception {
        // Assume that the ball is moving to the right
        gameplayView.ballX = gameplayView.screenWidth;
        gameplayView.velocity.setX(1);

        Method method = GameplayView.class.getDeclaredMethod("handleBallReachesBoundaries");
        method.setAccessible(true);
        method.invoke(gameplayView);

        assertEquals(-1, gameplayView.velocity.getX(), 0.01);
    }

    @Test
    public void testOnTouchEvent() {
        // Assume that the touch event is at the paddle's location
        MotionEvent event = MotionEvent.obtain(0, 0, MotionEvent.ACTION_DOWN, gameplayView.paddleX, gameplayView.paddleY, 0);
        assertTrue(gameplayView.onTouchEvent(event));

        event = MotionEvent.obtain(0, 0, MotionEvent.ACTION_MOVE, gameplayView.paddleX + 10, gameplayView.paddleY, 0);
        assertTrue(gameplayView.onTouchEvent(event));

        assertEquals(gameplayView.paddleX, gameplayView.oldPaddleX, 0.01);
    }

    @Test
    public void testHandleBallMissThePaddle() throws Exception {
        gameplayView.ballY = gameplayView.paddleY + gameplayView.paddle.getHeight() + 1;
        gameplayView.life = 2;

        Method method = GameplayView.class.getDeclaredMethod("handleBallMissThePaddle");
        method.setAccessible(true);
        method.invoke(gameplayView);

        assertEquals(1, gameplayView.life);
        assertFalse(gameplayView.gameOver);
    }

    @Test
    public void testHandleBallPaddleCollision() throws Exception {
        // Assume that the ball and paddle are at the same location
        gameplayView.ballX = gameplayView.paddleX;
        gameplayView.ballY = gameplayView.paddleY;

        Method method = GameplayView.class.getDeclaredMethod("handleBallPaddleCollision");
        method.setAccessible(true);
        method.invoke(gameplayView);

        assertEquals(40, gameplayView.velocity.getX(), 0.01);
        assertEquals(-34, gameplayView.velocity.getY(), 0.01);
    }

    @Test
    public void testHandleGameOver() throws Exception {
        gameplayView.points = 100;
        ShadowActivity shadowActivity = Shadows.shadowOf((Activity) context);

        Method method = GameplayView.class.getDeclaredMethod("handleGameOver");
        method.setAccessible(true);
        method.invoke(gameplayView);

        Intent startedIntent = shadowActivity.getNextStartedActivity();
        assertEquals(GameOver.class.getName(), startedIntent.getComponent().getClassName());
        assertEquals(100, startedIntent.getIntExtra("points", -1));
        assertTrue(gameplayView.gameOver);
    }

    @Test
    public void testRandomVelocityX() throws Exception {
        Method method = GameplayView.class.getDeclaredMethod("randomVelocityX");
        method.setAccessible(true);

        int velocityX = (int) method.invoke(gameplayView);
        assertTrue(Arrays.asList(-35, -30, -25, 25, 30, 35).contains(velocityX));
    }

    @Test
    public void testMoveBall() throws Exception {
        gameplayView.ballX = 10;
        gameplayView.ballY = 10;
        gameplayView.velocity.setX(5);
        gameplayView.velocity.setY(5);

        Method method = GameplayView.class.getDeclaredMethod("moveBall");
        method.setAccessible(true);
        method.invoke(gameplayView);

        assertEquals(15, gameplayView.ballX, 0.01);
        assertEquals(15, gameplayView.ballY, 0.01);
    }
}
